var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();
var coins = 0;
var lifes = 50;
$('#coins').text(coins);
$('#lifes').text(lifes);

var audioStart = $('#audioStart');
audioStart[0].play();

var audioGame = $('#audioGame');

var running = false;

var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$.fn.animationEnd = function(callback) {
    return this.each(function() {
        var $this = $(this);
        $this.bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
            if (typeof callback == 'function') {
               callback.call(this); // brings the scope to the callback
            };
        });
    })
 
};
var result;

var states = new Array();
states[3] = "-301px -468px";
states[1] = "-361px -468px";
states[2] = "-421px -468px";
states[0] = "-481px -468px";

$('html').on('mousedown', function () {
if(running == false) {
    Mediator.publish('inputGiven');
    running = true;
    audioGame[0].play();
    audioGame.on('ended', function() {
	audioGame[0].play();
    });
}
});

    
    
$('html').on('keydown', function () {
if(running == false) {
    Mediator.publish('inputGiven');
    running = true;
    audioGame[0].play();
    audioGame.on('ended', function() {
	audioGame[0].play();
    });
    }
});

Mediator.subscribe('datareceived', function (data) {
    result=data;
    if(coins == 0) {
        if(lifes > 0) {
	    lifes = lifes - 1;
	    $('#lifes').text(lifes);
	    coins = 99;
	}    
    }
    if(coins > 0) {
    	coins = coins - 1;
	$('#coins').text(coins);
	$('#left').addClass('left');
	$('#middle').addClass('middle');
	$('#right').addClass('right');
    }
});

$("#left").animationEnd(function(){
    $('#left').removeClass('left');
    $('#left').css('background-position', states[result[0].slots[0]]);
});

$("#middle").animationEnd(function(){
    $('#middle').removeClass('middle');
    $('#middle').css('background-position', states[result[0].slots[1]]);
});
var audioWin = $('#audioWin');
var audioLose = $('#audioLose');
function timeout_trigger() {
    if(result[0].result == 0){ 
	 audioLose[0].play();
	 alert(unescape("Du hast leider nichts gewonnen!")); 
    }
    else
    {
        audioWin[0].play();
	alert(unescape("Du hast " + result[0].result + " M%FCnzen gewonnen"));
    }

    coins = coins + result[0].result;
    if(coins > 99) {
      if(lifes < 99) {
        lifes = lifes + 1;
      	coins = coins - 99;
	 $('#lifes').text(lifes);
      }	 
      else
        coins = 99;
    }
       
    $('#coins').text(coins);
}

$("#right").animationEnd(function(){
    audioGame[0].pause();

    $('#right').removeClass('right');
    $('#right').css('background-position', states[result[0].slots[2]]);
    setTimeout('timeout_trigger()', 50);
    running = false;
});